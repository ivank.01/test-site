<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/catalog.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
    <title>Каталог</title>
</head>

<body>
    <header>
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Головна</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="catalog.php">Каталог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="auth.php">Увійти</a>
                </li>
            </ul>
            <form action="search.php" method="GET" class="d-flex">
                <input name="search" class="form-control me-2" type="search" placeholder="Пошук" value="
                <?php
                if (isset($_GET['search'])) {
                    echo $_GET['search'];
                }
                ?>" aria-label="Search">
                <button name="submit_search" class="btn btn-outline-primary bg-dark" require type="submit">Пошук</button>
            </form>
        </nav>
    </header>
    <div class="wrapper text-light">
        <div style="padding: 0 0 100px;">
            <div class="container">
                <h2 class="p-4">Дивитися фільми онлайн українською</h2>
                <div class="d-flex flex-row flex-wrap justify-content-around">

                    <?php
                    require('config/connect.php');

                    if (isset($_GET['catalog_page'])) {
                        $catalog_page = $_GET['catalog_page'];
                    } else {
                        $catalog_page = 1;
                    }
                    $notesOnPage = 10;
                    $from = ($catalog_page - 1) * $notesOnPage;


                    $query_film = mysqli_query($mysqli, "SELECT * FROM `films` LIMIT $from,$notesOnPage") or die(mysqli_error($mysqli));
                    $films = mysqli_fetch_all($query_film, MYSQLI_ASSOC);
                    foreach ($films as $elem) {
                    ?>
                        <a class="nav-link link-light" href="film.php?id=<?= $elem['id'] ?>">
                            <div>
                                <img src="<?= '/img/' . $elem['img'] ?>" class="mb-2" alt="123" width="220px" height="320px">
                                <p><?= $elem['name'] ?></p>
                            </div>
                        </a>
                    <?php
                    }

                    ?>
                </div>
            </div>

            <nav class="my-nav">
                <ul class="pagination d-flex justify-content-center text-primary p-4 mt-2">
                    <li class="page-item">
                    <?php
                    if($catalog_page != 1){
                        $prev = $catalog_page - 1;
    
                    ?>
                        <a class="page-link" href="<?= "catalog.php?catalog_page=" . $prev?>">Назад</a>
                        <?php
                    }
                    ?>
                    </li>
                    <?php
                    $query_film = mysqli_query($mysqli, "SELECT COUNT(*) as count FROM `films`") or die(mysqli_error($mysqli));
                    $count = mysqli_fetch_assoc($query_film)['count'];
                    $pagesCount = ceil($count / $notesOnPage);
                    for ($i = 1; $i <= $pagesCount; $i++) {
                    
                    ?>
                    <li class="page-item"><a class="page-link" href="?catalog_page=<?=$i?>"><?=$i?></a></li>
                    <?php
                    }
                    ?>
                    <li class="page-item">
                        <?php
                    if($catalog_page != $pagesCount){
                        $next = $catalog_page + 1;
    
                    ?>
                        <a class="page-link" href="<?= "catalog.php?catalog_page=" . $next?>">Вперед</a>
                        <?php
                    }
                    ?>
                    </li>
                </ul>
            </nav>
        </div>
    </div>


    <footer id="sticky-footer" class="flex-shrink-0 py-4 bg-dark text-primary">
        <div class="container text-center">
            <small>Copyright &copy; Your Website</small>
        </div>
    </footer>
</body>

</html>