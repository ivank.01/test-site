<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/film.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
    <?php
    require('config/connect.php');
    $film_id = $_GET['id'];
    $film = mysqli_query($mysqli, "SELECT * FROM `films` WHERE id = $film_id");
    $film = mysqli_fetch_all($film, MYSQLI_ASSOC);
    foreach ($film as $elem) {
    ?>
        <title><?= $elem['name'] ?></title>
    <?php
    }
    ?>
</head>

<body>
    <header>
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Головна</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="catalog.php">Каталог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="auth.php">Увійти</a>
                </li>
            </ul>
            <form action="search.php" method="GET" class="d-flex">
                <input name="search" class="form-control me-2" type="search" placeholder="Пошук"
                value="<?php
                if(isset($_GET['search'])){
                    echo $_GET['search'];
                }
                ?>" aria-label="Search">
                <button name="submit_search" class="btn btn-outline-primary bg-dark" require type="submit">Пошук</button>              
            </form>
        </nav>
    </header>
    <?php
    require_once('config/connect.php');
    $film_id = $_GET['id'];
    $film = mysqli_query($mysqli, "SELECT * FROM `films` WHERE id = $film_id");
    $film = mysqli_fetch_all($film, MYSQLI_ASSOC);
    foreach ($film as $elem) {
    ?>
        <div class="wrapper">
            <div class="container">
                <div class="my-content">
                    <h1 class="text-white text-center" name='name'><?= $elem['name'] ?></h1>

                    <div class="row">
                        <div class="col-sm-3">
                            <img src="<?= '/img/' . $elem['img'] ?>" alt="123" width="280px" height="360px">
                        </div>
                        <div class="text-white col-sm-9">
                            <p>Рейтинг: <?= $elem['rating']; ?></p>
                            <p>Дата виходу: <?= $elem['ex_date']; ?></p>
                            <p>Країна: <?= $elem['country']; ?></p>
                            <p>Про що фільм "<?= $elem['name']; ?>": <?= $elem['description']; ?></p>
                        </div>
                    </div>
                    <a href="<?= $elem['url']; ?>" class="text-decoration-none">
                        <button class="my-btn btn btn-lg btn-outline-primary d-grid gap-2 col-6 mx-auto">Перейти до перегляду</button>
                    </a>
                <?php
            }
                ?>
                </div>
            </div>

            <section>
                <div class="container my-5 py-5">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-12 col-lg-10">
                            <div class="card bg-dark text-white">
                                <form action="config/add_comments.php" method="post">
                                    <div class="card-footer py-3 border-0">
                                        <div class="d-flex flex-start w-100">
                                            <div class="form-outline w-100">
                                                <h3 class="mb-2">Залиште свій відгук</h3>
                                                <div class="my-rating" data-total-value="0">
                                                    <div class="my-rating-item" data-item-value="5">★</div>
                                                    <div class="my-rating-item" data-item-value="4">★</div>
                                                    <div class="my-rating-item" data-item-value="3">★</div>
                                                    <div class="my-rating-item" data-item-value="2">★</div>
                                                    <div class="my-rating-item" data-item-value="1">★</div>
                                                </div></br>
                                                <label class="form-label" for="textAreaExample">Ім'я</label>
                                                <p class="mb-n3"><input name="name" class="bg-light" type="text"><?= $name ?></p>
                                                <label class="form-label" for="textAreaExample">Повідомлення</label>
                                                <textarea name="review" class="form-control bg-light mb-3" id="textAreaExample" rows="4"><?= $review ?></textarea>
                                                <input type="hidden" value="<?=$film_id?>" name="film_id">
                                            </div>
                                        </div>
                                        <div class="float-end mt-2 pt-1">
                                            <button type="submit" name="submit" class="btn btn-primary btn-sm">Відправити</button>
                                        </div>
                                    </div>
                                </form>
                                <?php
                                require_once('config/connect.php');
                               
                                $query_comment = mysqli_query($mysqli, "SELECT * FROM `comments`") or die(mysqli_error($mysqli));
                                $query_comment = mysqli_fetch_all($query_comment, MYSQLI_ASSOC);
                                
                                foreach ($query_comment as $comment) {
                                    if($film_id === $comment['id_post']){
                                ?>
                                    <div class="card-body">
                                        <div>
                                            <h6 class="fw-bold text-primary mb-1"><?= $comment['name'] ?></h6>
                                            <p class="text-muted small mb-0">
                                                <?= $comment['date'] ?>
                                            </p>
                                        </div>

                                        <p class="mt-2 mb-3 pb-2">
                                            <?= $comment['review'] ?>
                                        </p>
                                    </div>
                                <?php
                                }
                            }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div></br>
        <footer id="sticky-footer" class="flex-shrink-0 py-4 bg-dark text-primary">
            <div class="container text-center">
                <small>Copyright &copy; Your Website</small>
            </div>
        </footer>
        <script src="js/script.js"></script>

</body>

</html>