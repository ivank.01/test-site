<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
    <title>Фільми українською</title>
</head>

<body>
    <div class="wrapper">
        <div class="content">

            <header>
                <nav class="navbar navbar-dark bg-dark">
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Головна</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="catalog.php">Каталог</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="auth.php">Увійти</a>
                        </li>
                    </ul>
                    <form action="search.php" method="GET" class="d-flex">
                        <input name="search" class="form-control me-2" type="search" placeholder="Пошук" value="
                        <?php
                        if (isset($_GET['search'])) {
                            echo $_GET['search'];
                        }
                        ?>" aria-label="Search">
                        <button name="submit_search" class="btn btn-outline-primary bg-dark" require type="submit">Пошук</button>
                    </form>
                </nav>
            </header>

            <content>
                <div class="container">
                    <h1>Новинки</h1>
                    <div class="my-content d-flex flex-row justify-content-around">

                        <?php
                        require('config/connect.php');
                        $query_film = mysqli_query($mysqli, "SELECT * FROM `films` WHERE id>0 LIMIT 0,4") or die(mysqli_error($mysqli));
                        $films = mysqli_fetch_all($query_film, MYSQLI_ASSOC);
                        foreach ($films as $elem) {
                        ?>
                            <a class="nav-link link-light" href="film.php?id=<?= $elem['id'] ?>">
                                <div class="my-content-film">
                                    <p><?= $elem['rating'] ?></p>
                                    <img src="<?= '/img/' . $elem['img'] ?>" alt="123" width="250px" height="360px">
                                    <p><?= $elem['name'] ?></p>
                                </div>
                            </a>
                        <?php
                        }

                        ?>
                    </div>
                </div>
            </content>
        </div>
    </div>
    <footer id="sticky-footer" class="flex-shrink-0 py-4 bg-dark text-primary">
        <div class="container text-center">
            <small>Copyright &copy; Your Website</small>
        </div>
    </footer>
</body>

</html>