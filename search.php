<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/film.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
    <title>Пошук</title>
</head>

<body>
    <header>
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Головна</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="catalog.php">Каталог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="auth.php">Увійти</a>
                </li>
            </ul>
            <form action="search.php" method="GET" class="d-flex">
                <input name="search" class="form-control me-2" type="search" placeholder="Пошук"
                value="<?php
                if(isset($_GET['search'])){
                    echo $_GET['search'];
                }
                ?>" aria-label="Search">
                <button name="submit_search" class="btn btn-outline-primary bg-dark" type="submit">Пошук</button>              
            </form>
        </nav>
    </header>
    <div class="wrapper">
    <div style="padding: 0 0 100px;">
        <div class="container">
            <h1 class="text-light p-4 m-3 text-center">Результати пошуку</h1>
            <div class="d-flex flex-row flex-wrap justify-content-evenly">
                <?php
                require_once('config/connect.php');
                if (isset($_GET['submit_search'])) {
                    $search = $_GET['search'];
                    $query_search = mysqli_query($mysqli, "SELECT * FROM `films` WHERE `name` LIKE '%$search%' ");
                    if (mysqli_num_rows($query_search) > 0) {
                        $query_search = mysqli_fetch_all($query_search, MYSQLI_ASSOC);
                        foreach ($query_search as $items) {
                ?>
                            <a class="nav-link link-light" href="film.php?id=<?= $items['id'] ?>">
                                <div class="m-3 text-center">
                                    <h5 class="mb-3"><?= $items['name'] ?></h5>
                                    <img src="<?= '/img/' . $items['img'] ?>" class="img-thumbnail mb-4 mt-2" alt="123" width="200px" height="300px">
                                </div>
                            </a>

                        <?php
                        }
                        ?>
                    <?php
                    } else {
                        echo '<h3 class="text-light m-3">Нічого не знайдено</h3>';
                    }
                }
                ?>

            </div>
        </div>
    </div>
    </div>
    <footer id="sticky-footer" class="flex-shrink-0 py-4 bg-dark text-primary">
        <div class="container text-center">
            <small>Copyright &copy; Your Website</small>
        </div>
    </footer>
</body>

</html>