const ratingItemsList = document.querySelectorAll('.my-rating-item');
const ratingItemsArray = Array.prototype.slice.call(ratingItemsList);

ratingItemsArray.forEach(element => 
    element.addEventListener('click', ()=>{
        const {itemValue} = element.dataset;
        element.parentNode.dataset.totalValue = itemValue;
    })
    
);

