<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/signup.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
    <title>Вхід</title>
</head>

<body>
    <header>
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Головна</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="catalog.php">Каталог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="auth.php">Увійти</a>
                </li>
            </ul>
            <form action="search.php" method="GET" class="d-flex">
                <input name="search" class="form-control me-2" type="search" placeholder="Пошук" value="
                <?php
                if (isset($_GET['search'])) {
                    echo $_GET['search'];
                }
                ?>" aria-label="Search">
                <button name="submit_search" class="btn btn-outline-primary bg-dark" require type="submit">Пошук</button>
            </form>
        </nav>
    </header>

    <div class="wrapper">
        <div class="content">
            <div class="container p-3">
                <div class="row">
                    <div class="my-form text-light m-auto col-md-offset-3 col-md-6 p-4 mt-4">
                        <form class="form-horizontal" action="config/signup.php" method="POST">
                            <h3 class="heading p-2">Реєстрація</h3>
                            <label>ПІБ</label>
                            <p><input type="text" name="full_name" class="form-control p-2 my-2" placeholder="Введіть повне ім'я"></p>
                            <label>Логін</label>
                            <p><input type="text" name="login" class="form-control p-2 my-2" placeholder="Введіть логін"></p>
                            <label>Пошта</label>
                            <p><input type="email" name="email" class="form-control p-2 my-2" placeholder="Введіть електронну пошту"></p>
                            <label>Пароль</label>
                            <p><input type="password" name="password" class="form-control p-2 my-2" placeholder="Введіть пароль"></p>
                            <label>Підтвердження паролю</label>
                            <input type="password" name="password_confirm" class="form-control p-2 my-2" placeholder="Введіть пароль ще раз">
                            <button type="submit" name="submit" class="btn btn-outline-primary bg-dark my-3">Зареєструватись</button>
                            <p>
                                У вас вже є акаунт? <a href="auth.php" style="text-decoration: none;color: #0d6efd;">Авторизуйтесь</a>
                            </p>
                            <?php
                                if($_SESSION['message']){
                                    echo '<p class="my-msg text-light border border-2 rounded">' . $_SESSION['message'] . '</p>';
                                }
                                unset($_SESSION['message']);
                                ?>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <footer id="sticky-footer" class="flex-shrink-0 py-4 bg-dark text-primary">
        <div class="container text-center">
            <small>Copyright &copy; Your Website</small>
        </div>
    </footer>

</body>

</html>