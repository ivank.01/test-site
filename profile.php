<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/signup.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
    <title>Особистий кабінет</title>
</head>

<body>
    <header>
        <nav class="navbar navbar-dark bg-dark">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Головна</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="catalog.php">Каталог</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="auth.php">Увійти</a>
                </li>
            </ul>
            <form action="search.php" method="GET" class="d-flex">
                <input name="search" class="form-control me-2" type="search" placeholder="Пошук" value="
                <?php
                if (isset($_GET['search'])) {
                    echo $_GET['search'];
                }
                ?>" aria-label="Search">
                <button name="submit_search" class="btn btn-outline-primary bg-dark" require type="submit">Пошук</button>
            </form>
        </nav>
    </header>
    <div class="wrapper text-light">
        <div class="content">
            <div class="container py-2">
                <div class="row">
                    <div class="col-md-11 mt-3">
                        <h2 class="p-2">Особистий кабінет</h2>
                    </div>
                    <a class="btn btn-outline-light col-md-1 p-3 mt-4" href="config/logout.php">Вихід</a>
                </div>
                <p class="pb-3 pt-4">ПІБ: <?= $_SESSION['user']['full_name'] ?></p>
                <p class="py-3">Логін: <?= $_SESSION['user']['login'] ?></p>
                <p class="py-3">Email: <?= $_SESSION['user']['email'] ?></p>

            </div>
        </div>
    </div>

    <footer id="sticky-footer" class="flex-shrink-0 py-4 bg-dark text-primary">
        <div class="container text-center">
            <small>Copyright &copy; Your Website</small>
        </div>
    </footer>
</body>

</html>